﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App12
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private const int ITEM_OFFSET = -48;
        private int _lastIndex;
        private bool _isInitalized;

        private  void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipView = sender as FlipView;

            if (_isInitalized == false)
            {
                foreach (var item in flipView.Items)
                {
                    (item as FlipViewItem).RenderTransform = new CompositeTransform();
                }
                _isInitalized = true;
            }

            var selectedIndex = flipView.SelectedIndex;

            var currentElement = flipView.Items.ElementAt(selectedIndex) as FrameworkElement;

            if (selectedIndex >= flipView.Items.Count - 1)
            {

                AnimateOffset(0, currentElement, -48);
                return;
            }

            var nextElement = flipView.Items.ElementAt(selectedIndex + 1) as FrameworkElement;
            if (nextElement != null)
            {

                if (_lastIndex < selectedIndex) // do przodu
                {
                    AnimateOffset(0, currentElement, -48);
                    AnimateOffset(ITEM_OFFSET, nextElement);
                }
                else // do tyłu
                {
                    AnimateOffset(ITEM_OFFSET, nextElement);
                }
            }

            _lastIndex = selectedIndex;
        }

        public void AnimateOffset(double endValue, DependencyObject target, double startValue = double.NaN)
        {
            Storyboard changeOffsetStoryboard = new Storyboard();

            DoubleAnimation changeOffsetAnimation = new DoubleAnimation
            {
                To = endValue,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 200))
            };

            if (double.IsNaN(startValue) == false)
            {
                changeOffsetAnimation.From = startValue;
            }

            changeOffsetStoryboard.Children.Add(changeOffsetAnimation);
            Storyboard.SetTarget(changeOffsetAnimation, target);
            Storyboard.SetTargetProperty(changeOffsetAnimation, "(UIElement.RenderTransform).(CompositeTransform.TranslateX)");

            changeOffsetStoryboard.Begin();
        }
    }
}
